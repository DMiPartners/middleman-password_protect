# all in one for now
require 'middleman-core'
require 'securerandom'
require 'fileutils'
require 'digest'

# not really clear if this should live in ::Middlman
class PasswordProtect < ::Middleman::Extension
  option :form_template, nil, 'path to custom form template, relative to build directory'
  option :files, [], 'an array of strings which are file globs ["blog/*", "secret.html", ...] '
  option :life, 1, 'how many days should a good password be kept (cookie life)'
  option :users, nil, 'a hash of username => password pairs to allow viewing of protected pages'
  expose_to_config :encrypt

  def initialize(app, options_hash = {}, &block)
    super

    exit(1) unless good_options(options)

    require 'gibberish'

    @_generated_form = false
  end

  # get js to be built, include form template too if it's not set;
  # maybe proxy through to the sitemap instead would be cleaner
  def before_build(builder)
    include_js
    builder.thor.say_status :protected, 'added js to source for processing', :green

    return if options.form_template

    include_form
    builder.thor.say_status :protected, 'added form to source for processing', :green
    @_generated_form = true
  end

  def after_build(builder)
    filesfound = []

    options.files.each do |item|
      filesfound.concat generate_filelist(item, builder)
    end

    filesfound.each do |file|
      hidepage! file, SecureRandom.hex
      builder.thor.say_status :protected, file.sub(build_dir + File::SEPARATOR, ''), :green
    end

    cleanup
  end

  # not used yet
  def encrypt(files, pass = nil, template = nil, life = 1)
    return @filelist unless files

    @filelist.push [files, pass || options.password, template || options.form_template, life]
  end

  def generate_filelist(path, builder)
    good = []
    begin
      build_path = File.join build_dir, path

      Dir.glob(build_path) do |file|
        good << file if File.file?(file) && !File.zero?(file)
      end
    rescue StandardError => e # can we be more specific
      builder.thor.say_status e.message
    end

    good
  end

  def hidepage!(file, pass)
    cipher = Gibberish::AES.new(pass)
    crypted_page = cipher.encrypt IO.read(file).to_s

    FileUtils.remove_file file
    IO.write file, generate_cover(formfile_data, crypted_page, pass)
  end

  def generate_cover(wrapping, encrypted, pass)
    wrapping.sub! 'ENCRYPTEDPAGE', encrypted.to_json
    wrapping.sub! 'COOKIED', Digest::MD5.hexdigest(options.files.to_json)
    wrapping.sub! 'DAYSTOLIVE', options.life.to_s
    wrapping.sub! 'PPUSERS', hash_users(options.users, pass).to_s.gsub('=>', ':')
    wrapping
  end

  def include_js
    FileUtils.copy_file(
      File.join(File.expand_path('../../source/assets', __dir__), 'sjcl.js'),
      sjcl_sourcepath
    )
  end

  # make a random name for the template to avoid anything that may exist
  def include_form
    options.form_template = Gibberish::MD5(Time.now) + '.html'

    FileUtils.copy_file(
      File.join(File.expand_path('../../source', __dir__), 'login_form.html.erb'),
      File.join(app.source_dir, options.form_template + '.erb')
    )
  end

  def cleanup
    FileUtils.rm_f [sjcl_sourcepath, formfile_buildpath]

    return unless @_generated_form

    FileUtils.remove_file(File.join(app.source_dir, options.form_template + '.erb'))
    options.form_template = nil
    @_generated_form = false
  end

  def build_dir
    File.join app.root, app.config.build_dir
  end

  def formfile_data
    return @_formfile_data if @_formfile_data

    @_formfile_data = File.read formfile_buildpath
  end

  def formfile_buildpath
    File.join build_dir, options.form_template
  end

  def sjcl_sourcepath
    File.join(app.source_dir, app.config.js_dir, 'sjcl.js')
  end

  # Internal: make a list of users/passes with sjcl compatible hashes;
  #   or encrypting a password with other passwords
  #
  # users - a hash of usernames => password pairs
  # pass - the password to encrypt with the users password
  #
  # Returns a hash of user => hash_string where hash_string is the encrypted pass and user is definitely a string
  def hash_users(users, pass)
    newusers = {}
    users.keys.each do |key|
      newusers[key.to_s] = Gibberish::AES.new(users[key]).encrypt(pass).to_s
    end
    newusers
  end

  # Internal: check given initialization options to make sure we can use them
  #
  # opts - a Middleman.options object
  #
  # Returns boolean false is something is amiss, otherwise true
  def good_options(opts)
    unless opts.files && opts.files.is_a?(Array)
      logger.error '== PassProtect: files config is not an array'
      return false
    end

    unless opts.users && opts.users.is_a?(Hash)
      logger.error '== PassProtect: users config is not a hash'
      return false
    end

    true
  end
end
