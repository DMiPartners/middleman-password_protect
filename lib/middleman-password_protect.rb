require 'middleman-core'

Middleman::Extensions.register :passwordprotect do
  require 'middleman-password_protect/extension'
  PasswordProtect
end
