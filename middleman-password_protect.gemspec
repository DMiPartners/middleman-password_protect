$LOAD_PATH.push File.expand_path('lib', __dir__)

Gem::Specification.new do |gem|
  gem.name        = 'middleman-password_protect'
  gem.version     = '0.2.3'
  gem.platform    = Gem::Platform::RUBY
  gem.authors     = ['DMi Partners']
  gem.email       = ['dantep@dmipartners.com']
  gem.homepage    = 'https://www.dmipartners.com/'
  gem.summary     = 'revisited password protection of Middleman pages'
  gem.description = 'Add authorization to view middleman generated pages with some encryption'

  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.executables   = `git ls-files -- bin/*`.split("\n").map { |f| File.basename(f) }
  gem.require_paths = ['lib']

  gem.add_runtime_dependency('middleman-core', ['>= 4.2.1'])

  gem.add_dependency('gibberish', '>= 2')
end
