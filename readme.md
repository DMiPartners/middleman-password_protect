# Middleman Password Protection

An extention to password protect groups of pages in [Middleman](https://middlemanapp.com). Inspired by, and somewhat forked from, [middleman-gibberish](https://github.com/ahoward/middleman-gibberish). With more options and less jquery, what more could anyone want?

Files are encrypted during (right after) building and can be "viewed" from any browser on any platform by way of javascript decryption AS LONG AS THEY are viewed from a server.

### Sample Config

```ruby
# activate the extenstion
  activate :passwordprotect do |art|
  # set the users and their passwords
    art.users = { 'alice': 'jabberwock', 'carpenter': 'walrus' }

  # set array of file globs to protect
    art.files = ['blog/**/*.html', 'a/single/file.html']

  # set your own custom login template (see source/login_form.html.erb) cause the default one is ugly
    art.form_template = 'login.html'

  # set how long an accepted password is remembered
    art.life = 1
  end

```

### Notes

- ALL paths in the configuration (files, form_template) are relative to the BUILD directory, not the source
- Which means, customization should simply go with your site build process, as templates and javascript files are added to your source (and removed after building)
- This extension isn't used in development mode

### To Do
- docutize-ation
- allow groups of pages to have different passwords
- actually use a username and password
- allow for more than one user/pass for a protected set
- maybe a test or two

### Dependencies

middleman-passwordprotect uses:
- [gibberish gem](https://github.com/mdp/gibberish)
- [SJCL](https://github.com/bitwiseshiftleft/sjcl/)
- [JS Cookie](https://github.com/js-cookie/js-cookie)
